/*
    My drawing is based on the concept of 'Autunomous Agents and Fractals'. I have taken inspiration from the code provided to us in class and from Daniel Shiffman's videos. 
    
    The following drawing has a half koch curve snowflake in the middle of the screen. When the mouse is Pressed, the koch curve displays the next generation of the same. Eventually if you keep pressing, the system becomes slower as the koch curve is being drawn with the help of a recursive function. 
    
    Other than the Koch Curve, we can see three different set of boids on the screen. 
    1. Gold Fish - These boids are in the form of fish and can only move in one direction. They are also the only ones that experience both alignment and cohesion. 
    2. Purple color boids - These boids have a random velocity and they only experience Alignment. 
    3. White Boids - These boids do not experience cohesion as well. 
    The White and Purple Boids eventually become stagnant. 
    
    I tried to make the boids interact with each other as well but it didn't get the desired result. 
*/

var goldfish = [];
var jellyfish = [];
var stars = [];
var segments = [];

var alignSlider, cohesionSlider, separationSlider;
function setup()
{
    createCanvas(windowWidth,windowHeight);
    
    //pushing a new boid in each of the arrays
    for( var i = 0; i < 20; i++)
    {
        goldfish.push(new Boid(1));
        jellyfish.push(new Boid(0));
        stars.push(new Boid(0));
    }
    
    //KOCH CURVE
    // start and end point of the line 
    var start = createVector(0, 300);
    var end = createVector(600, 300);
    //drawing the first line segment and then using recursion, splitting it
     var seg1 = new Koch_Curve(start, end);

    var seglen = p5.Vector.dist(start, end);
    var h = seglen * sqrt(5) / 2.5;
    
    //height of the koch curve
    var c = createVector(300, 100+h);
  
    //drawing the second line segment and then using recursion, splitting it
    var seg2 = new Koch_Curve(end, c);
    
    //drawing the third line segment and then using recursion, splitting it
    var seg3 = new Koch_Curve(c, start);
    segments.push(seg1);
    segments.push(seg2);
    segments.push(seg3);

}

function draw()
{
    background(0);
    //using for loop to make the boid work 
    
    //the golden fish
    for(var i = 0; i < goldfish.length; i++)
    {
        goldfish[i].edges();
        goldfish[i].flocks(goldfish);
        goldfish[i].update();
        goldfish[i].show(218,165,32, 0);
    } 
    
    //the purple colored circles 
    for(var i = 0; i < jellyfish.length; i++)
    {
        jellyfish[i].edges();
        jellyfish[i].flocks(jellyfish);
        jellyfish[i].update();
        jellyfish[i].show(255,25,255, 1);
    } 
    
    //the white floating points 
    for(var i = 0; i < stars.length; i++)
    {
        stars[i].edges();
        stars[i].flocks(stars);
        stars[i].update();
        stars[i].show(255,255,255, 2);
    } 
    
    push();
    //translate(windowWidth * 0.31, windowHeight * 0.15);
    //translating the koch curve to the center of the screen 
    translate(windowWidth * 0.3, -windowHeight * 0.15);
    //gicing it a random color 
    stroke(random(0,255), random(0,255), random(0,255));
    for (var i=0; i < segments.length; i++) 
    {
        segments[i].display();
    }
    pop();
}


function addAll(arr, list) {
  for (var i = 0; i < arr.length; i++) {
    list.push(arr[i]);
  }
}

//using mousePressed function to go to the next generation of the koch curve 
function mousePressed() {
  var nextGeneration = [];

  for (var i = 0; i < segments.length; i++) {
    var child = segments[i].generate();
    addAll(child, nextGeneration);
  }
  segments = nextGeneration;
}


//using the mouse Dragged function so that the user can make more boids 
function mouseDragged() {
//    if(key == 'g')
//    {
        goldfish.push(new Boid(1));
//    }
//    else if( key == 'j')
//    {
        jellyfish.push(new Boid(0));
//    }
//    else if(key == 's')
//    {
        stars.push(new Boid(0));
        
//    }
}


/*defining a constructor function called boid*/ 
function Boid(vel) {
    this.position = createVector(random(width), random(height));
    /*making sure that the fish only flow in one direction whereas everything else has a random direction*/
    if(vel == 1)
        this.velocity = createVector(random(windowWidth), random(height));
    else
        this.velocity = p5.Vector.random2D();
    this.velocity.setMag(random(2,4));
    this.acceleration = createVector();
    this.maxForce = 0.5;
    this.maxSpeed = 4;
    //this.i = 0;
    
    /* Making sure that once the boid reaches the end of the screen (0 or width or height), it wraps around the screen and shows from the other side*/
    this.edges = function(){
        if(this.position.x > width)
        {
            this.position.x = 0;    
        }
        else if(this.position.x < 0)
        {
            this.position.x = width;
        }
        if(this.position.y > height)
        {
            this.position.y = 0;
        }
        else if(this.position.y < 0)
        {
            this.position.y = height;
        }
    }
    
/*ALIGNMENT:
    Steer towards the average heading of the neighbours*/
    this.align = function(boids) {
        var perceptionRadius = 100;
        var steering = createVector();
        var total = 0;
        //for(var i = 0; i < boids.length; i++)
        for(var i = 0; i < boids.length; i++)
        {
            var d = dist(
                this.position.x, 
                this.position.y, 
                boids[i].position.x, 
                boids[i].position.y);
            
            if(boids[i] != this && d < perceptionRadius)
            {
                steering.add(boids[i].velocity);
                total++;
            }
        }
        
        if(total > 0)
        {
            steering.div(total);
            /*setting a fixed speed so that they dont slow down and instead keep going at a faster pace making sure that the alignment happens faster */ 
            steering.setMag(8);
            //this.velocity = avg;
            steering.sub(this.velocity);
            //giving a negative value to steering so that it can have a random behaviour and not do the cohesion bit - this is applicable to all except the fishes
            if(vel != 1)
                steering.mult(-10);
            
            steering.limit(this.maxForce);
        }
        return steering;
    }
    

/*COHESION: 
    Steer to move toward the average position of the local flockamtes*/
    this.cohesion  = function(boids) {
        var perceptionRadius = 100;
        var steering = createVector();
        var total = 0;
        //for(var i = 0; i < boids.length; i++)
        for(var i = 0; i < boids.length; i++)
        {
            var d = dist(
                this.position.x, 
                this.position.y, 
                boids[i].position.x, 
                boids[i].position.y);
            
            if(boids[i] != this && d < perceptionRadius)
            {
                steering.add(boids[i].velocity);
                total++;
            }
        }
        
        if(total > 0)
        {
            steering.div(total);
            /*setting a fixed speed so that they dont slow down and instead keep going at a faster pace making sure that the alignment happens faster */ 
            steering.setMag(this.maxSpeed);
            //this.velocity = avg;
            steering.sub(this.velocity);
            steering.limit(this.maxForce);
        }
        return steering;
    }
    
    
    this.flocks = function(boids) {
        
        /* COHESION AND ALLIGNMENT:
        Force Accumulation - two forces are acting on an object then the resulting acceleration is the sum of those forces 
        We do not want the acceleration to accumulate over time and at given time we want the net acceleration to start from 0 */ 
        this.acceleration.mult(0);   
        var alignment = this.align(boids);
        var cohesion = this.cohesion(boids);
        this.acceleration.add(alignment);
        this.acceleration.add(cohesion);   
        
    }
    
    this.update = function() {
        this.position.add(this.velocity);
        this.velocity.add(this.acceleration);
        this.velocity.limit(this.maxSpeed);
    }
    
    this.show = function(r,g,b, shape){
        push();
        strokeWeight(7);
        //stroke(this.i - 100, this.i % 175, this.i % 105);
        //fill(0);
        
        //drawing a specific shape based on the parameter passed 
        if(shape == 0)
        {
            stroke(r,g,b);
            fill(r,g,b);
            triangle(this.position.x - 20, this.position.y, this.position.x - 20, this.position.y -5, this.position.x - 20, this.position.y +5);
            ellipse(this.position.x, this.position.y, 30, 10);
            
            //ellipse(this.position.x + 10, this.position.y , 10, 10);
            //line(this.position.x, this.position.y, this.position.x + 10, this.position.y);
            //point(this.position.x + 9, this.position.y);
            //bezier(250, 250, 0, 100, 100, 0, 100, 0, 0, 0, 100, 0);
            //bezier(250, 250, 0, 100, 100, 0, 100, 0, 0, 250, 250, 0);
        }
        else if(shape == 1)
        {
            stroke(r,g,b);
            ellipse(this.position.x, this.position.y, 10, 10);
            push();
            translate(this.position.x, this.position.y);
            line(10,10,10,10);
            //triangle(0,10,10,10,10,10);
            pop();
            //rect(this.position.x, this.position.y, 10, 10)
        }
        else if(shape == 2)
        {
            stroke(r,g,b);
            point(this.position.x, this.position.y)
        }
        pop();
    }
    
}




function Koch_Curve(a,b) 
{
    this.start = a.copy();
    this.stop = b.copy();
  
    this.generate = function() 
    {
        var split = [];
        //difference between the two end points
        var v = p5.Vector.sub(this.stop, this.start);
        //splitting into 3 parts 
        v.div(3);
        
        //-----    ------    
        //     \  /
        //      \/
        //  A  B  C  D
        
        // Segment A
        var a_end = p5.Vector.add(this.start, v);
        split[0] = new Koch_Curve(this.start, a_end);

        // Segment D
        var d_start = p5.Vector.sub(this.stop, v);
        split[3] = new Koch_Curve(d_start, this.stop);
        
        //rotating by 60 degrees for line B 
        v.rotate(PI/3);
        var top = p5.Vector.add(a_end,v);
        // Segment B
        split[1] = new Koch_Curve(a_end, top);
        
        // Segment C
        split[2] = new Koch_Curve(top, d_start);
        return split;
    }
  
    this.display = function()
    {
        stroke(random(255), random(255), random(255));
        line(this.start.x, this.start.y, this.stop.x, this.stop.y);
    }
}
